package com.csdn.gateway.feign;

/**
 * @author StoneHkx
 * @ClassName userFeign
 * @Description TODO
 * @createDate 2020-01-05 18:29
 * @updatePerson
 * @updateDate
 */

import com.csdn.gateway.feign.fallback.UserFeignHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(value = "userservice",fallback = UserFeignHystrix.class)
public interface UserFeign {

    @PostMapping("/user/getUserByNamePassword")
    Map<String,Object> getUserByNamePassword(@RequestBody Map<String,String> params);

}
