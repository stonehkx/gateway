package com.csdn.gateway.controller;

import com.alibaba.fastjson.JSON;
import com.csdn.gateway.feign.UserFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author StoneHkx
 * @ClassName LoginControler
 * @Description TODO
 * @createDate 2020-01-05 19:16
 * @updatePerson
 * @updateDate
 */
@RestController
public class LoginControler {

    @Autowired
    private UserFeign userFeign;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @PostMapping("/login")
    public Map<String,Object> login(@RequestBody Map<String,String> params){
        Map<String,Object> returnMap = new HashMap<>();

        String userName = params.get("userName");
        String password = params.get("password");

        if(StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)){
            returnMap.put("msg","用户名或密码不能为空");
            return returnMap;
        }

        Map<String,String> map = new HashMap<>();
        map.put("userName",userName);
        map.put("password",password);
        try {
            Map<String, Object> userByNamePassword = userFeign.getUserByNamePassword(map);
            Boolean result = (Boolean) userByNamePassword.get("result");
            if(!result){
                returnMap.put("msg",(String)userByNamePassword.get("data"));
                return returnMap;
            }

            if(!StringUtils.isEmpty(userByNamePassword.get("data"))){
                Map data = JSON.parseObject((String) userByNamePassword.get("data"), Map.class);
                if(!StringUtils.isEmpty(data.get("userName")) && !StringUtils.isEmpty(data.get("password"))){

                    String token = UUID.randomUUID().toString();

                    stringRedisTemplate.opsForValue().set(token,(String) userByNamePassword.get("data"),60*10, TimeUnit.SECONDS);
                    //String s = stringRedisTemplate.opsForValue().get(token);
                    Map data1 = JSON.parseObject((String) userByNamePassword.get("data"), Map.class);
                    returnMap.put("msg",(String)userByNamePassword.get("data"));
                    returnMap.put("token",token);
                    returnMap.put("result","登录成功");
                    return returnMap;
                }
            }
        }catch (Exception e){
            returnMap.put("result",e.getMessage());
            return returnMap;
        }

        returnMap.put("msg","登录失败");
        return returnMap;
    }

    @PostMapping("/logout")
    public String logout(@RequestBody Map<String,String> params){
        String token = params.get("TOKEN");
        if(StringUtils.isEmpty(token)){
            return "登出参数不能为空";
        }
        Boolean delete = stringRedisTemplate.delete(token);
        if(!StringUtils.isEmpty(delete) && delete){
            return "登出成功";
        }else {
            return "登出失败";
        }
    }
}
