package com.csdn.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author StoneHkx
 * @ClassName LoginFilter
 * @Description TODO
 * @createDate 2020-01-05 21:10
 * @updatePerson
 * @updateDate
 */
@Component
public class LoginFilter extends ZuulFilter {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /*
        被转发之前执行
     */
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {

        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();

        String token = request.getHeader("TOKEN");

        if(StringUtils.isEmpty(token)){
            currentContext.setResponseBody("TOKEN不能为空");
            currentContext.setResponseStatusCode(200);
            currentContext.setSendZuulResponse(false);
        }

        if(!getUserInfo(token)){

            /*currentContext.setResponseBody("succesful");
            currentContext.setResponseStatusCode(200);
            currentContext.setSendZuulResponse(true);
        }else{*/
            currentContext.setResponseBody("取不到用户信息");
            currentContext.setResponseStatusCode(200);
            currentContext.setSendZuulResponse(false);
        }

        return null;
    }

    private boolean getUserInfo(String token){

        String userInfo= stringRedisTemplate.opsForValue().get(token);

        if(StringUtils.isEmpty(userInfo)){
            return false;
        }

        return true;

    }
}
